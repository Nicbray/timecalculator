//
//  ViewController.swift
//  DistanceCalculator
//
//  Created by Nicole Braybrook on 26/9/17.
//  Copyright © 2017 Nicole Braybrook. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // always keep track of Diameter, RPM and Distance in Millimeters
    
    var diameter = 0.0
    var rpm = 0.0;
    var distance = 0.0;
    
    var diameterInMeters = 0.0
    var frequencyInRpm = 0.0
    var distanceInMeters = 0.0
    
    // other current variables
    var time = 0.0;
    var rotations = 0.0;
    
    var justPushed = "0";
    
    // the text fields we can edit
    enum editingOptions {
        case diameter
        case frequency
        case distance
    }
     // All outlets
    var currentlyEditing = editingOptions.diameter
    
    // diameter currently is in
    enum diameterUnits {
        case millimeters
        case meters
        case inches
        case feet
    }
        var diameterIs = diameterUnits.millimeters

    //frequency currently is in
    enum frequencyUnits {
        case rpm
        case hertz
    }

    var frequencyIs = frequencyUnits.rpm
    
    //distnce currently is in
    enum distanceUnits {
        case millimeters
        case meters
        case inches
        case feet
    }
    
    var distanceIs = distanceUnits.millimeters
    
    //distnce currently is in
    enum timeUnits {
        case milliseconds
        case seconds
        case minutes
        case hours
    }
    
    var timeIs = timeUnits.milliseconds

    
    @IBOutlet weak var segmentedControl_diameter: UISegmentedControl!

    @IBOutlet weak var textField_diameter: UITextField!
    @IBOutlet weak var segmentedControl_rpm: UISegmentedControl!
    
    @IBOutlet weak var textField_rpm: UITextField!
    
    @IBOutlet weak var segmentedControl_distance: UISegmentedControl!
    
    @IBOutlet weak var textField_distance: UITextField!
    
    @IBOutlet weak var segmentedControl_time: UISegmentedControl!
    
    @IBOutlet weak var textField_time: UITextField!
    
    @IBOutlet weak var textField_rotations: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // is string an integer?
    func isStringAnInt(string: String) -> Bool {
        return Int(string) != nil
    }
    
    //is string a decimal?
    func isStringADot(string: String) -> Bool {
        if string == "." {
            return true
        } else {
            return false
        }
    }
    
    // checks if string a valid number, and cleans it up (removes extra decimal points)
    func isStringANumber(string:String) -> String {
        
        var decimalPoints = 0
        
        var newString = ""
        
        for i in string.characters {
            if isStringADot(string: String(i)) == true {
                decimalPoints = decimalPoints + 1;
                if decimalPoints == 1  {
                    newString = newString + ".";
                }
            } else if isStringAnInt(string: String(i)) == true {
                newString = newString + String(i);
            }
        }
        
        if (newString == ""){
            return "0"
        } else {
        return newString
        }
    }
    
    
    // returns true if there is a decimal in the string
    func doesNumberHaveADecimal(string:String) -> Bool {
        
        for i in string.characters {
            if isStringADot(string: String(i)) == true {
                return true
                }
        }
        
        return false
    }
    
    // takes a string, and returns that string without any decimals
    func removeDecimals(string:String) -> String {
        
        var newString = ""
        
        for i in string.characters {
            if isStringAnInt(string: String(i)) == true {
                newString = newString + String(i);
            }
        }
        
        if (newString == ""){
            return "0"
        } else {
            return newString
        }
        
    }
    


    
    func editingEnded_distance() {
        
        
    }
    
    @IBAction func selectedSegmentChanged_diameter(_ sender: Any) {
        currentlyEditing = editingOptions.diameter

        
        let diameterWas = diameterIs;
        
        print("got hre to 1")
        
        if segmentedControl_diameter.selectedSegmentIndex == 0 {
            print("got hre to 2")

            diameterIs = diameterUnits.millimeters
            
            print("got hre to 3")

            // Millimeters
            
            if (diameterWas == diameterUnits.millimeters){
            
                // do nothing
            
            } else if (diameterWas == diameterUnits.meters){
                
                print("got hre to 4")

                // meters to millimeters
                
                print("got hre to 5")

                textField_diameter.text! = String(Double(textField_diameter.text!)! * 1000)
                
            } else if (diameterWas == diameterUnits.inches){
                
                // inches to millimeters
                
                textField_diameter.text! = String(Double(textField_diameter.text!)! * 25.4)
            } else {
                
                // feet to millimeters
                
                textField_diameter.text! = String(Double(textField_diameter.text!)! * 304.8)
            }
            
        } else if segmentedControl_diameter.selectedSegmentIndex == 1 {
            // Meters

            diameterIs = diameterUnits.meters
            
            
            if (diameterWas == diameterUnits.millimeters){
                
                // millimeters to meters
                
                textField_diameter.text! = String(Double(textField_diameter.text!)! * 0.001)
                
            } else if (diameterWas == diameterUnits.meters){
                
                 // do nothing
                
            } else if (diameterWas == diameterUnits.inches){
                
                // inches to meters
                
                textField_diameter.text! = String(Double(textField_diameter.text!)! * 0.0254)
            } else {
                // feet to meters
                textField_diameter.text! = String(Double(textField_diameter.text!)! * 0.3048)
            }
            
        } else if segmentedControl_diameter.selectedSegmentIndex == 2 {
            // Inches
            
            diameterIs = diameterUnits.inches
            
            
            if (diameterWas == diameterUnits.millimeters){
                
                // millimeters to inches
                
                textField_diameter.text! = String(Double(textField_diameter.text!)! * 0.03937)
                
            } else if (diameterWas == diameterUnits.meters){
                
                // meters to inches
                
                textField_diameter.text! = String(Double(textField_diameter.text!)! * 39.370079)
                
            } else if (diameterWas == diameterUnits.inches){
                
                // do nothing
                
            } else {
                // feet to inches
                textField_diameter.text! = String(Double(textField_diameter.text!)! * 12)
            }

        } else if segmentedControl_diameter.selectedSegmentIndex == 3 {
            diameterIs = diameterUnits.feet
            
            
            if (diameterWas == diameterUnits.millimeters){
                
                // millimeters to feet
                
                textField_diameter.text! = String(Double(textField_diameter.text!)! * 0.003281)
                
            } else if (diameterWas == diameterUnits.meters){
                
                // meters to feet
                
                textField_diameter.text! = String(Double(textField_diameter.text!)! * 3.28084)
                
            } else if (diameterWas == diameterUnits.inches){
                
                // inches to feet
                
                textField_diameter.text! = String(Double(textField_diameter.text!)! * 0.083333)
            } else {
                // do nothing
            }
        }

    }
    @IBAction func valueChanged_diameter(_ sender: Any) {
        
        
       // ensure the user hasnt manually input nubers or letters first 
        
        let text = isStringANumber(string: textField_diameter.text!)
        
        textField_diameter.text = text
        

        diameter = Double(text)!

        
    }
    

    @IBAction func selectedSegmentChanged_rpm(_ sender: Any) {
        
        currentlyEditing = editingOptions.frequency
        
        let frequencyWas = frequencyIs;
        

        if segmentedControl_rpm.selectedSegmentIndex == 0 {
            // RPM
            if (frequencyWas == frequencyUnits.rpm){
                
                // do nothing
                
            } else {
                
                // hertz to RPM
                textField_rpm.text! = String(Double(textField_rpm.text!)! * 60)
                
            }
        
        } else if segmentedControl_rpm.selectedSegmentIndex == 1 {
            if (frequencyWas == frequencyUnits.rpm){
                
                // hrtz to RPM             //hertz = rpm / 60
                // hertz to RPM
                textField_rpm.text! = String(Double(textField_rpm.text!)! / 60)
                
            } else {
                
                // do nothing
                
            }
        }
    }

    @IBAction func valueChanged_rpm(_ sender: Any) {
        
        
        // ensure the user hasnt manually input nubers or letters
        
        let text = isStringANumber(string: textField_rpm.text!)
        
        textField_rpm.text = text
        
        rpm = Double(text)!
        
    }
    
    @IBAction func selectedSegmentChanged_distance(_ sender: Any) {
        
        currentlyEditing = editingOptions.distance
        
        let distanceWas = distanceIs;
        
        
        if segmentedControl_distance.selectedSegmentIndex == 0 {
            
            distanceIs = distanceUnits.millimeters
            
            // Millimeters
            
            if (distanceWas == distanceUnits.millimeters){
                
                // do nothing
                
            } else if (distanceWas == distanceUnits.meters){
                
                
                // meters to millimeters
                
                textField_distance.text! = String(Double(textField_distance.text!)! * 1000)
                
            } else if (distanceWas == distanceUnits.inches){
                
                // inches to millimeters
                
                textField_distance.text! = String(Double(textField_distance.text!)! * 25.4)
            } else {
                
                // feet to millimeters
                
                textField_distance.text! = String(Double(textField_distance.text!)! * 304.8)
            }
            
        } else if segmentedControl_distance.selectedSegmentIndex == 1 {
            // Meters
            
            distanceIs = distanceUnits.meters
            
            
            if (distanceWas == distanceUnits.millimeters){
                
                // millimeters to meters
                
                textField_distance.text! = String(Double(textField_distance.text!)! * 0.001)
                
            } else if (distanceWas == distanceUnits.meters){
                
                // do nothing
                
            } else if (distanceWas == distanceUnits.inches){
                
                // inches to meters
                
                textField_distance.text! = String(Double(textField_distance.text!)! * 0.0254)
            } else {
                // feet to meters
                textField_distance.text! = String(Double(textField_distance.text!)! * 0.3048)
            }
            
        } else if segmentedControl_distance.selectedSegmentIndex == 2 {
            // Inches
            
            distanceIs = distanceUnits.inches
            
            
            if (distanceWas == distanceUnits.millimeters){
                
                // millimeters to inches
                
                textField_distance.text! = String(Double(textField_distance.text!)! * 0.03937)
                
            } else if (distanceWas == distanceUnits.meters){
                
                // meters to inches
                
                textField_distance.text! = String(Double(textField_distance.text!)! * 39.370079)
                
            } else if (distanceWas == distanceUnits.inches){
                
                // do nothing
                
            } else {
                // feet to inches
                textField_distance.text! = String(Double(textField_distance.text!)! * 12)
            }
            
        } else if segmentedControl_distance.selectedSegmentIndex == 3 {
            distanceIs = distanceUnits.feet
            
            
            if (distanceWas == distanceUnits.millimeters){
                
                // millimeters to feet
                
                textField_distance.text! = String(Double(textField_distance.text!)! * 0.003281)
                
            } else if (distanceWas == distanceUnits.meters){
                
                // meters to feet
                
                textField_distance.text! = String(Double(textField_distance.text!)! * 3.28084)
                
            } else if (distanceWas == distanceUnits.inches){
                
                // inches to feet
                
                textField_distance.text! = String(Double(textField_distance.text!)! * 0.083333)
            } else {
                // do nothing
            }
        }


    }

    
    @IBAction func valueChanged_distance(_ sender: Any) {
        
 
        let text = isStringANumber(string: textField_distance.text!)
        
        
        textField_distance.text = text
        
        distance = Double(text)!;
        
    }

    
    @IBAction func valueChanged_time(_ sender: Any) {
        
        let timeWas = timeIs;
        
        
        if segmentedControl_time.selectedSegmentIndex == 0 {
            
            timeIs = timeUnits.milliseconds
            
            // Milliseconds
            
            if (timeWas == timeUnits.milliseconds){
                
                // do nothing
                
            } else if (timeWas == timeUnits.seconds){
                
                
                // seconds to millisecs
                
                textField_time.text! = String(Double(textField_time.text!)! * 1000)
                
            } else if (timeWas == timeUnits.minutes){
                
                // minutes to millisecs
                
                textField_time.text! = String(Double(textField_time.text!)! * 60000)
            }
            
        } else if segmentedControl_time.selectedSegmentIndex == 1 {
            // Meters
            
            timeIs = timeUnits.seconds
            
            
            if (timeWas == timeUnits.milliseconds){
                
                // millsecs to seconds
                
                textField_time.text! = String(Double(textField_time.text!)! * 0.001)
                
            } else if (timeWas == timeUnits.seconds){
                
                // do nothing
                
            } else if (timeWas == timeUnits.minutes){
                
                // minutes to seconds
                
                textField_time.text! = String(Double(textField_time.text!)! * 60)
            }
            
        } else if segmentedControl_time.selectedSegmentIndex == 2 {
            // Inches
            
            timeIs = timeUnits.minutes
            
            
            if (timeWas == timeUnits.milliseconds){
                
                // millisecs to minutes
                
                textField_time.text! = String(Double(textField_time.text!)! * 1.66667e-5)
                
            } else if (timeWas == timeUnits.seconds){
                
                // seconds to minutes
                
                textField_time.text! = String(Double(textField_time.text!)! * 0.0166667)
                
            } else if (timeWas == timeUnits.minutes){
                
                // do nothing
                
            }
            
        }

        
        
        if segmentedControl_time.selectedSegmentIndex == 0 {
            // Millimeters
            textField_time.text = String(time)
        } else if segmentedControl_time.selectedSegmentIndex == 1 {
            // seconds = milliseconds ÷ 1,000
            textField_time.text = String(Double(time) / 1000)
        } else if segmentedControl_time.selectedSegmentIndex == 2 {
            // minutes = ((milliseconds / 1000) / 60)
            
            textField_time.text = String((Double(time) / 1000) / 60)
            
        } else if segmentedControl_time.selectedSegmentIndex == 3 {
            // Hours = (((milliseconds / 1000) / 60) /60)
            textField_time.text = String(((Double(time) / 1000) / 60) / 60)
        }
    }
    
    // checks if all text fields are empty or zero
    func isEmptyOrZero() -> Bool {
        
        // checks all fields

        if textField_diameter.text! != "0" || textField_rpm.text! != "0" || textField_distance.text! != "0" {

                
                let diameterText = isStringANumber(string: textField_diameter.text!)
                let diameterVal = Int(removeDecimals(string: diameterText))!
                
                let rpmText = isStringANumber(string: textField_rpm.text!)
                let rpmVal = Int(removeDecimals(string: rpmText))!
                
                let distanceText = isStringANumber(string: textField_diameter.text!)
                let distanceVal = Int(removeDecimals(string: distanceText))!
            
            
            if (diameterVal == 0 || rpmVal == 0 || distanceVal == 0){
                return true;
            } else {
                return false;
            }
                
                
            
        } else {
            
            return true
        }
        
        
    }
    
    // begin editing
    
    @IBAction func clickedOn_diameter(_ sender: Any) {
        currentlyEditing = editingOptions.diameter;
    }
    
    @IBAction func clickedOn_rpm(_ sender: Any) {
        currentlyEditing = editingOptions.frequency;
    }
    
    @IBAction func currenlyEditing_distance(_ sender: Any) {
        currentlyEditing = editingOptions.distance;
    }
    
    
    
    @IBAction func touchUpInside_numbers(_ sender: UIButton) {
    }
    
    // if any key 1,2,3,4,5,6,7,8,9,0,. is pushed
    @IBAction func keyPressed(_ sender: UIButton) {
        
        justPushed = sender.titleLabel!.text!;

        if (currentlyEditing == editingOptions.diameter){
            
            textField_diameter.text! += justPushed

            valueChanged_diameter(sender: sender)
            
        } else if (currentlyEditing == editingOptions.frequency){
            
            textField_rpm.text! += justPushed
            
            valueChanged_rpm(sender: sender)
            
        } else {
            textField_distance.text! += justPushed
            
            valueChanged_distance(sender: sender)
        }
        
        checkIfAllFieldsAreFilled()
    }

    // if < is pushed
    @IBAction func backspacePressed(_ sender: Any) {
        // IMPLEMENT
        if (currentlyEditing == editingOptions.diameter){
            
            if (textField_diameter.text! != "0" && textField_diameter.text! != ""){
                textField_diameter.text
                    = textField_diameter.text!.substring(to: textField_diameter.text!.index(before: textField_diameter.text!.endIndex))
            }
        } else if (currentlyEditing == editingOptions.frequency){

            if (textField_rpm.text! != "0" && textField_rpm.text! != ""){
                textField_rpm.text
                = textField_rpm.text!.substring(to: textField_rpm.text!.index(before: textField_rpm.text!.endIndex))
                }
        } else {
                
                if (textField_distance.text! != "0" && textField_distance.text! != ""){
            textField_distance.text = textField_distance.text!.substring(to: textField_distance.text!.index(before: textField_distance.text!.endIndex))
                }}
    }
    
    // check if all are filled, if so - we can process the time and rotations
    func checkIfAllFieldsAreFilled(){
        
        
        if (!isEmptyOrZero() && distance > 0 && rpm > 0 && distance > 0){
            
            getAsMeters()

            
            // Distance (metres) / (((pi * Diameter (metres) ) / 60) * RPM)
            
            time = distanceInMeters / (((3.14 * diameterInMeters) / 60) * frequencyInRpm)
                
            
            print ("time is " + String(time))
            
            textField_time.text = String(time)
            
        }
        
        checkForRotations()
        
    }
    
    // chck if we can calialate the total rotations needed
    func checkForRotations(){
        
        if (rpm > 0 && distance > 0){
            
            getAsMeters()

            
            // we can work out total rotations needed.
                        // show that it is in seo

            segmentedControl_time.selectedSegmentIndex = 1
            segmentedControl_time.sendActions(for: UIControlEvents.valueChanged)
            
            // find the circumference of a circle just by multiplying the diameter by pi
            
            let circumference = Double(Double(diameterInMeters) * 3.14)
            
            //Number of rotations - divide the distance by the wheel circumference.
            
            rotations = Double(Double(distanceInMeters) / circumference)
            
            textField_rotations.text = String(rotations)
            
            
        }
        
    }

    
    func getAsMeters(){
        // Distance (metres) / (((pi * Diameter (metres) ) / 60) * RPM) 
        
        
        // record was they were before
        
        segmentedControl_diameter.selectedSegmentIndex = 1
        segmentedControl_diameter.sendActions(for: UIControlEvents.valueChanged)
        
        segmentedControl_rpm.selectedSegmentIndex = 0;
        segmentedControl_rpm.sendActions(for: UIControlEvents.valueChanged)

        
        segmentedControl_distance.selectedSegmentIndex = 1
        segmentedControl_distance.sendActions(for: UIControlEvents.valueChanged)

        
        diameterInMeters = Double(textField_diameter.text!)!
        frequencyInRpm = Double(textField_rpm.text!)!
        distanceInMeters = Double(textField_distance.text!)!
        
    }
    
}

